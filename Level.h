#ifndef _LEVEL_H
#define _LEVEL_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>


#include "Tile.h"
#include "Tee.h"
#include "Cup.h"
#include "Warp.h"
#include "util.h"



class Level {

    std::vector<Tile*> levelTiles;
    Tee levelTee;
    Cup levelCup;
    std::vector<Warp> levelWarps;



    void parseTile(const std::vector<std::string>& tokens) {
        if (tokens.size() < 3) {
            std::cerr << "Level.parseTile: incorrect number of tokens" << std::endl;
            return;
        }

        int n_vertices = atoi(tokens[2].c_str());

        if (tokens.size() < (3 + 3 * n_vertices + n_vertices)) {
            std::cerr << "Level.parseTile: incorrect number of tokens" << std::endl;
            return;
        }

        Tile* tileRef = new Tile(tokens);
        this->addTile(tileRef);
        return;
    }

    void parseTee(const std::vector<std::string>& tokens) {
        if (tokens.size() < 5) {
            std::cerr << "Level.parseTee: incorrect number of tokens" << std::endl;
            return;
        }

        Tee tee = Tee(tokens);
        this->setTee(tee);
        return;
    }

    void parseCup(const std::vector<std::string>& tokens) {
        if (tokens.size() < 5) {
            std::cerr << "Level.parseCup: incorrect number of tokens" << std::endl;
            return;
        }

        Cup cup = Cup(tokens);
        this->setCup(cup);
        return;
    }

    void parseWarp(const std::vector<std::string>& tokens) {
        if (tokens.size() < 9) {
            std::cerr << "Level.parseWarp: incorrect number of tokens" << std::endl;
            return;
        }

        Warp warp = Warp(tokens);
        this->addWarp(warp);
        return;
    }

public:

    Level() {
        this->levelTiles = std::vector<Tile*>();
        this->levelTee = Tee();
        this->levelCup = Cup();
        this->levelWarps = std::vector<Warp>();
    }

    void parseLines(const std::vector<std::string>& lines) {
        std::vector<std::string> tokens;

        for (int iter = 0; iter < lines.size(); ++iter) {
            split(lines[iter], ' ', tokens);

            if (tokens.size() < 1) continue;

            if (tokens[0].compare("tile") == 0) parseTile(tokens);
            if (tokens[0].compare("tee") == 0) parseTee(tokens);
            if (tokens[0].compare("cup") == 0) parseCup(tokens);
            if (tokens[0].compare("warp") == 0) parseWarp(tokens);

            tokens.clear();
        }

        this->sort();
    }

    void addTile(Tile* t) {
		levelTiles.push_back(t);
    }

	void sort(){
		std::vector<Tile*> inorder(levelTiles.size() + 1);
		for(int i = 0; i < levelTiles.size(); ++i){
			int index = levelTiles[i]->getID();
			inorder[index] = levelTiles[i];
		}

		levelTiles = inorder;
		
	}

    const Tile& getTile(int tileID) const {

		if(tileID <= 0 || tileID > levelTiles.size()){
			std::cerr << "Level.getTile: tile ID of " << tileID << " not found" << std::endl;
			return nullTile;
		}		

		return *levelTiles[tileID];
    }

    void setTee(const Tee& t) {
        this->levelTee = t;
    }

    const Tee& getTee() const {
        return this->levelTee;
    }

    void setCup(const Cup& c) {
        this->levelCup = c;
    }

    const Cup& getCup() const {
        return this->levelCup;
    }

    void addWarp(const Warp& w) {
        levelWarps.push_back(w);
    }

    const Warp& getWarp(unsigned warpIndex) const {
        if (warpIndex > this->levelWarps.size()) {
            std::cerr << "Level.getWarp: provided warpIndex is out of bounds" << std::endl;
            return nullWarp;
        }

        if (warpIndex == 0) std::cerr << "Level.getWarp: warpIndex should be in range 1 to n" << std::endl;
        else warpIndex -= 1;

        return this->levelWarps[warpIndex];
    }

    unsigned getTileCount() const {
        return this->levelTiles.size() - 1;
    }

    unsigned getWarpCount() const {
        return this->levelWarps.size();
    }

    ~Level() {

    }
};



#endif