#include "Cup.h"

#include <cstdlib>

Cup::Cup() {
    this->tileID = 0;
    this->position = glm::vec3(0.f);
}

Cup::Cup(int tileID, float x, float y, float z) {
    this->tileID = tileID;
    this->position.x = x;
    this->position.y = y;
    this->position.z = z;
}

Cup::Cup(int tileID, const glm::vec3& position) {
    this->tileID = tileID;
    this->position = position;
}

Cup::Cup(const std::vector<std::string>& tokens) {
    this->tileID = atoi(tokens[1].c_str());
    this->position.x = atof(tokens[2].c_str());
    this->position.y = atof(tokens[3].c_str());
    this->position.z = atof(tokens[4].c_str());
}

Cup& Cup::operator=(const Cup& rhs) {
    this->tileID = rhs.tileID;
    this->position = rhs.position;

    return *this;
}

void Cup::parseLine(const std::vector<std::string>& tokens) {
    *this = Cup(tokens);
}

int Cup::getTileID() const {
    return this->tileID;
}

const glm::vec3& Cup::getPosition() const {
    return this->position;
}

void Cup::clear() {
    *this = Cup();
}
