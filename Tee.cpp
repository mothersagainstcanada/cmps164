#include "Tee.h"

#include <cstdlib>

Tee::Tee() {
    this->tileID = 0;
    this->position = glm::vec3(0.f);
}

Tee::Tee(int tileID, float x, float y, float z) {
    this->tileID = tileID;
    this->position.x = x;
    this->position.y = y;
    this->position.z = z;
}

Tee::Tee(int tileID, const glm::vec3& position) {
    this->tileID = tileID;
    this->position = position;
}

Tee::Tee(const std::vector<std::string>& tokens) {
    this->tileID = atoi(tokens[1].c_str());
    this->position.x = atof(tokens[2].c_str());
    this->position.y = atof(tokens[3].c_str());
    this->position.z = atof(tokens[4].c_str());
}

Tee& Tee::operator=(const Tee& rhs) {
    this->tileID = rhs.tileID;
    this->position = rhs.position;

    return *this;
}

void Tee::parseLine(const std::vector<std::string>& tokens) {
    *this = Tee(tokens);
}

int Tee::getTileID() const {
    return this->tileID;
}

const glm::vec3& Tee::getPosition() const {
    return this->position;
}

void Tee::clear() {
    *this = Tee();
}
