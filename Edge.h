#ifndef _EDGE_H
#define _EDGE_H

#include "util.h" // includes GLM, string, vector

//taken straight from the UML

class Edge {
    glm::vec3 pointA, pointB;
public:
    Edge(const glm::vec3& _pointA, const glm::vec3& _pointB);
    glm::vec3 intersection(const Edge&);
    bool collides(const Edge&);
    glm::vec3 NearestPoint(const glm::vec3& _point) const;

    const glm::vec3& getPointA() const;
    const glm::vec3& getPointB() const;

    void setPointA(const glm::vec3&);
    void setPointB(const glm::vec3&);

    glm::vec3 getNormal() const;
};

#endif
