To run, minigolf.exe and associated DLL's will be provided.  The command is 'minigolf hole.##.db'.
In the event the program crashes, the likely culprit is the MinGW DLL's libgcc_s_dw2-1.dll and libstdc++-6.dll.
Try recompiling and replacing the provided MinGW DLL's with those in your MinGW bin folder.

To compile the program, run the compile script compileFileParse.bat on Windows.
This assumes a version of MinGW is installed.

This program depends on the GLUT library (given as glut.h, glut32.lib, glut32.dll).
It also depends on the GLM library (found in the subfolder glm, glm.hpp).

The file fileParse.cpp contains the GLUT window setup and level rendering code.
While parsing the file, the assumption is made that tile ID's are within the range [1-n],
where n is the number of tiles.  They can be provided out of order (sorted in implementation).

To move the camera, use the keys AD keys to move along the X axis, QE to move along Y,
and WS to move along Z.  Clicking and dragging the mouse will rotate the camera.

--Improvments--

Swap camera modes with the 1, 2, and 3 keys.
The first mode is the free movement explained above.
The second mode is overhead: use WASD to scroll over the level and QE to zoom.
The third is third-person: follows the ball!  Use click-drag to rotate and WS to zoom.

--Improvments Phase 2--

At the start of a hole, the ball is able to be hit.
Press the [ or ] key to rotate the hit vector around.
Press the < or > keys to skip around levels (not recommended).
Time the pressing of the Space key to vary the hit speed (see the red guide line).
Once hit, the ball will roll until it stops on a flat surface; then it may be hit again.
Current total score, score for current hole, par of current hole, and the current stroke count are displayed.
