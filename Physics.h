#ifndef _PHYSICS_H
#define _PHYSICS_H

class Physics {
    static int ballTileID;

    static float gravity;
    static float friction;
    static float impact;

    Physics() {} // no instances allowed!

public:
    static void update(float dt);

    static int getBallTileID();
    static void setBallTileID(int ID);

    static float getGravity();
    static void setGravity(float gravity);

    static float getFriction();
    static void setFriction(float friction);

    static float getImpact();
    static void setImpact(float impact);
};

#endif
