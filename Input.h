#ifndef _INPUT_H
#define _INPUT_H

/*
    I guess static functions do not use the const-ending convention for read-only
*/

class Input {
    static bool keyPress[256];

    static int mouseX;
    static int mouseY;
    static int mouseDX;
    static int mouseDY;

    Input() {} // no instances allowed!

public:
    static void keyboardFunc(unsigned char key, int x, int y);
    static void keyboardUpFunc(unsigned char key, int x, int y);
    static void motionFunc(int x, int y);
    static void passiveMotionFunc(int x, int y);

    static bool getKeyPress(unsigned char key);

    static int getMouseX();
    static int getMouseY();
    static int getMouseDX();
    static int getMouseDY();

    static void clearMouseDiff();
};

#endif
