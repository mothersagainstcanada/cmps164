#ifndef _GAMEMANAGER_H
#define _GAMEMANAGER_H

#include <vector>
#include <string>

#include "Ball.h"
#include "Camera.h"
#include "Level.h"
#include "highscore.h"

class Ball;
class Camera;
class Level;

class GameManager {
    static Ball* ball;
    static Camera* camera;

    static std::vector<Level> levels;
    static std::vector<unsigned> pars;
    static unsigned currentLevel;
	static int score;
	static int TotalScore;
	static int hole_on_1;
	static int on_par;
	static std::string playername;

	static unsigned hit;
	

    static bool ballMoving;
    static float hitAngle;
    static float hitPower;

    static bool gameDone;

    GameManager() {} // no instances allowed!

public:
    static Ball& getBall();
    static void setBall(Ball& ball);

	static void setParScore(int, int);

    static Camera& getCamera();
    static void setCamera(Camera& camera);

    static void loadLevel(const std::string& filePath);
    static void loadCourse(const std::string& filePath);

    static Level& getCurrentLevel();
    static void setCurrentLevel(unsigned holeNumber);

    static unsigned getCurrentPar();
    static unsigned getLevelPar(unsigned holeNumber);

	static void setParseScore(int max, int min);

	static int getScore();
	static int getTotalScore();
	static std::string getPlayerName();

	static void setPlayerName(std::string);

	static unsigned hitCount();

    static unsigned getLevelCount();
    static Level& getLevel(unsigned holeNumber);

    static bool isBallMoving();
    static float getHitAngle();
    static float getHitPower();

    static bool isGameDone();
    static void resetGame();

    static void enterNextLevel();
    static void enterPreviousLevel();

	static int getCurrentPossibleScore();

    static void updateGame(float dt);
};

#endif
