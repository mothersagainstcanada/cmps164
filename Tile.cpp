#include "Tile.h"

#include <cstdlib>
#include <iostream>

Tile::Tile() {
    this->ID = this->vertexCount = 0;
    this->vertices = std::vector<glm::vec3>();
    this->neighbors = std::vector<int>();
}

Tile::Tile(const std::vector<std::string>& tokens) {
    this->ID = atoi(tokens[1].c_str());
    this->vertexCount = atoi(tokens[2].c_str());

    this->vertices = std::vector<glm::vec3>(this->vertexCount);

    for (int iter = 0; iter < this->vertexCount; iter += 1) {
        int tokenOffset = 3 + 3 * iter;

        this->vertices[iter].x = atof(tokens[tokenOffset + 0].c_str());
        this->vertices[iter].y = atof(tokens[tokenOffset + 1].c_str());
        this->vertices[iter].z = atof(tokens[tokenOffset + 2].c_str());
    }

    this->neighbors = std::vector<int>(this->vertexCount);

    for (int iter = 0; iter < this->vertexCount; iter += 1) {
        int tokenOffset = 3 + 3 * this->vertexCount + iter;

        this->neighbors[iter] = atoi(tokens[tokenOffset + 0].c_str());
    }
}

void Tile::parseLine(const std::vector<std::string>& tokens) {
    if (this->vertexCount != 0) {
        this->vertices.clear();
        this->neighbors.clear();
    }

    *this = Tile(tokens);
}

int Tile::getID() const {
    return this->ID;
}

unsigned Tile::getVertexCount() const {
    return this->vertexCount;
}

const glm::vec3& Tile::getVertex(unsigned index) const {
    if (index > this->vertices.size()) {
        std::cerr << "Tile.getVertex: index out of range" << std::endl;
        return zeroVector;
    }

    return this->vertices[index];
}

void Tile::getVertices(std::vector<glm::vec3>& output) const {
    output = this->vertices;
}

int Tile::getNeighbor(unsigned index) const {
    if (index > this->neighbors.size()) {
        std::cerr << "Tile.getNeighbor: index out of range" << std::endl;
        return 0;
    }

    return this->neighbors[index];
}

void Tile::getNeighbors(std::vector<int>& output) const {
    output = this->neighbors;
}

void Tile::clear() {
    this->vertices.clear();
    this->neighbors.clear();

    this->vertexCount = this->ID = 0;
}
