#include "highscore.h"

using namespace std;

priority_queue<player_score, std::vector<player_score>, compare_score> prioq;

priority_queue<player_score, std::vector<player_score>, compare_score> hi_score::pq = prioq;

player_score hello [10];
player_score hi_score::hi_array[10] = hello;


// read the file and create a priority queue

void hi_score::readfile(string filename){
	ifstream file;

	file.open(filename.c_str(), ios::in);

	if(file.fail()) {cerr << "Cannot open file" << endl; return;}

	for(string line; getline(file, line);){
		istringstream stream(line);

		string token;
		int score;
		stream >> token >> score;

		if(stream.fail()){
			cerr << "Invalid score format" << endl;
		}

		hi_score::pq.push(player_score(token, score));
	}

	file.close();
}

// write from sorted array of high scores to file

void hi_score::writefile(){
	remove("high_score.db");

	ofstream write;
	write.open("high_score.db");

	for(int i = 0; i < 10; ++i){
		write << hi_array[i].getName() << " " << hi_array[i].getScore() << endl;
	}

	write.close();

}


