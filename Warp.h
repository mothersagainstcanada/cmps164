#ifndef _WARP_H
#define _WARP_H

#include "util.h" // includes GLM, string, vector

class Warp {
    int tileIDa, tileIDb;

    glm::vec3 positionA, positionB;

public:
    Warp();
    Warp(int tileIDa, const glm::vec3& positionA, int tileIDb, const glm::vec3& positionB);
    Warp(const std::vector<std::string>& tokens);

    Warp& operator=(const Warp& rhs);

    void parseLine(const std::vector<std::string>& tokens);

    int getTileIDa() const;
    const glm::vec3& getPositionA() const;

    int getTileIDb() const;
    const glm::vec3& getPositionB() const;

    void clear();
};

const Warp nullWarp = Warp(); // added for ability to return a const reference (see Level.getWarp)

#endif
