#ifndef _RENDER_H
#define _RENDER_H

#include "Camera.h"

#include "glut.h"

#include "glm/glm.hpp"

#include "Input.h"
#include "Tile.h"
#include "Tee.h"
#include "Cup.h"
#include "Warp.h"
#include "util.h"
#include "Level.h"
#include "Camera.h"
#include "Ball.h"
#include "GameManager.h"
#include "HUD.h"


class Render{
	static Camera View;
	static float lightpos[4];

public:
	static void init(float, float, float, float);
	static void cb_display();

};


#endif