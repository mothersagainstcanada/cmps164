#include "Input.h"

bool Input::keyPress[256] = {false};

int Input::mouseX = 0;
int Input::mouseY = 0;
int Input::mouseDX = 0;
int Input::mouseDY = 0;


// various utilities function to be provided to glut.

void Input::keyboardFunc(unsigned char key, int x, int y) {
    Input::keyPress[key] = true;
}

void Input::keyboardUpFunc(unsigned char key, int x, int y) {
    Input::keyPress[key] = false;
}

void Input::motionFunc(int x, int y) {
    Input::mouseDX = x - Input::mouseX;
    Input::mouseDY = y - Input::mouseY;

    Input::mouseX = x;
    Input::mouseY = y;
}

void Input::passiveMotionFunc(int x, int y) {
    Input::mouseX = x;
    Input::mouseY = y;
}

bool Input::getKeyPress(unsigned char key) {
    return Input::keyPress[key];
}

int Input::getMouseX() {
    return Input::mouseX;
}

int Input::getMouseY() {
    return Input::mouseY;
}

int Input::getMouseDX() {
    return Input::mouseDX;
}

int Input::getMouseDY() {
    return Input::mouseDY;
}

void Input::clearMouseDiff() {
    Input::mouseDX = 0;
    Input::mouseDY = 0;
}
