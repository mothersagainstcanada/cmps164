#include "GameManager.h"

#include <iostream>
#include <fstream>

#include <cmath>

#include "Physics.h"
#include "Input.h"

Ball* GameManager::ball = 0;
Camera* GameManager::camera = 0;

// default player name is Player1
std::string GameManager::playername = "Player1";

std::vector<Level> GameManager::levels = std::vector<Level>();
std::vector<unsigned> GameManager::pars = std::vector<unsigned>();
unsigned GameManager::currentLevel = 0;
unsigned GameManager::hit = 1;
int GameManager::score = 0;
int GameManager::TotalScore = 0;

int GameManager::hole_on_1 = 200;
int GameManager::on_par = 50;

bool GameManager::ballMoving = false;
float GameManager::hitAngle = 0.f;
float GameManager::hitPower = 1.f;

bool GameManager::gameDone = false;

bool hitDisable = false;
unsigned recentWarp = 0;

std::string GameManager::getPlayerName(){
	return playername;
}

void GameManager::setPlayerName(std::string s){
	GameManager::playername = s;
}

glm::vec3 getTileNormal2(const Tile& tile) {
    glm::vec3 AB = tile.getVertex(1) - tile.getVertex(0);
    glm::vec3 AC = tile.getVertex(2) - tile.getVertex(0);

    return glm::normalize(glm::cross(AB, AC));
}

// set ball on the position of the tee and stop the velocity.
void setBallOnTee() {
    Ball& ball = GameManager::getBall();
    Level& level = GameManager::getCurrentLevel();

    glm::vec3 ballCenter = level.getTee().getPosition();
    ballCenter.y += ball.getGeometry().getRadius();

    int teeTile = level.getTee().getTileID();
    Physics::setBallTileID(teeTile);

    ball.setCenter(ballCenter);
    ball.setVelocity(glm::vec3(0.f));

    recentWarp = 0;
}


Ball& GameManager::getBall() {
    return *(GameManager::ball);
}

void GameManager::setBall(Ball& ball) {
    GameManager::ball = &(ball);
    if (GameManager::levels.size() > 0) setBallOnTee();
}

void GameManager::setParScore(int max, int min){
	GameManager::hole_on_1 = max;
	GameManager::on_par = min;
}



Camera& GameManager::getCamera() {
    return *(GameManager::camera);
}

void GameManager::setCamera(Camera& camera) {
    GameManager::camera = &(camera);
}



void GameManager::loadLevel(const std::string& filePath) {
    std::string line;
    std::ifstream file(filePath.c_str());

    std::vector<std::string> levelLines;
    std::vector<std::string> tokens;

    if (!file.is_open()) {
        std::cerr << "GameManager::loadLevel: unable to open file " << filePath << std::endl;
        return;
    }

    while (std::getline(file, line)) { // find first line with tokens
        split(line, ' ', tokens);
        if (tokens.size() > 0) break;
    }

    if (tokens[0].compare("course") == 0) { // course keyword found
        std::cerr << "GameManager::loadLevel: file " << filePath << " is a course; please use loadCourse" << std::endl;
        return;
    }

    tokens.clear();

    do {
        split(line, ' ', tokens);
        if (tokens.size() < 1) continue;

        levelLines.push_back(line);

        tokens.clear();
    } while (std::getline(file, line));

    Level temp = Level();
    temp.parseLines(levelLines);
    GameManager::levels.push_back(temp);

    if (GameManager::ball != 0) setBallOnTee();
}

void GameManager::loadCourse(const std::string& filePath) {
    std::string line;
    std::ifstream file(filePath.c_str());

    std::vector<std::string> levelLines;
    std::vector<std::string> tokens;

    bool readingLevel = false;

    if (!file.is_open()) {
        std::cerr << "GameManager::loadCourse: unable to open file " << filePath << std::endl;
        return;
    }

    while (std::getline(file, line)) { // find first line with tokens
        split(line, ' ', tokens);
        if (tokens.size() > 0) break;
    }

    if (tokens[0].compare("course") != 0) {
        std::cerr << "GameManager::loadCourse: course specifier not found in file " << filePath << std::endl;
    }

    tokens.clear();

    do {
        split(line, ' ', tokens);
        if (tokens.size() < 1) continue;

        if (tokens[0].compare("begin_hole") == 0) readingLevel = true;

        if (tokens[0].compare("end_hole") == 0) {
            readingLevel = false;
            Level temp = Level();
            temp.parseLines(levelLines);
            GameManager::levels.push_back(temp);
            levelLines.clear();
        }

        if (readingLevel) levelLines.push_back(line);

        if (tokens[0].compare("par") == 0) {
            if (tokens.size() < 2) {
                std::cerr << "GameManager::loadLevel: par not specified" << std::endl;
            } else if (atoi(tokens[1].c_str()) == 0) {
                std::cerr << "GameManager::loadLevel: invalid par (or par 0)" << std::endl;
            } else {
                GameManager::pars.push_back(atoi(tokens[1].c_str()));
            }
        }

        tokens.clear();
    } while (std::getline(file, line));

    if (GameManager::ball != 0) setBallOnTee();
}



Level& GameManager::getCurrentLevel() {
    if (GameManager::currentLevel >= GameManager::levels.size()) {
        std::cerr << "GameManager::getCurrentLevel: current level not found." << std::endl;
    }

    return (GameManager::levels[GameManager::currentLevel]);
}

void GameManager::setCurrentLevel(unsigned holeNumber) {
    holeNumber -= 1; // this is to allow input of 1 to levels.size()

    if (holeNumber >= GameManager::levels.size()) {
        std::cerr << "GameManager::setCurrentLevel: hole number of " << holeNumber << " not found" << std::endl;
        return;
    }

    GameManager::currentLevel = holeNumber;
    if (GameManager::ball != 0) setBallOnTee();
}


unsigned GameManager::getCurrentPar() {
    if (GameManager::currentLevel >= GameManager::levels.size()) {
        std::cerr << "GameManager::getCurrentLevel: current level not found." << std::endl;
    }

    return (GameManager::pars[GameManager::currentLevel]);
}

unsigned GameManager::getLevelPar(unsigned holeNumber) {
    holeNumber -= 1; // this is to allow input of 1 to levels.size()

    if (holeNumber >= GameManager::levels.size()) {
        std::cerr << "GameManager::setCurrentLevel: hole number of " << holeNumber << " not found" << std::endl;
        return 0;
    }

    return (GameManager::pars[holeNumber]);
}

int GameManager::getScore(){
	return GameManager::score;
}

int GameManager::getTotalScore(){
	return GameManager::TotalScore;
}

unsigned GameManager::getLevelCount() {
    return GameManager::levels.size();
}

Level& GameManager::getLevel(unsigned holeNumber) {
    holeNumber -= 1; // this is to allow input of 1 to levels.size()

    if (holeNumber >= GameManager::levels.size()) {
        std::cerr << "GameManager::getLevel: hole number of " << holeNumber << " not found" << std::endl;
        return GameManager::levels[GameManager::currentLevel];
    }

    return GameManager::levels[holeNumber];
}


bool GameManager::isBallMoving() {
    return GameManager::ballMoving;
}

float GameManager::getHitAngle() {
    return GameManager::hitAngle;
}

float GameManager::getHitPower() {
    return GameManager::hitPower;
}

bool GameManager::isGameDone() {
    return GameManager::gameDone;
}

void GameManager::resetGame() {
    GameManager::gameDone = false;
    GameManager::currentLevel = 0;
    GameManager::TotalScore = 0;
    GameManager::hit = 1;
    hitDisable = true;
}

void GameManager::enterNextLevel() {
    if (GameManager::currentLevel + 1 >= levels.size()) {
        std::cerr << "GameManager::enterNextLevel: unable to find next level" << std::endl;
        return;
    }

	GameManager::ballMoving = false;
	GameManager::hit = 1;
	GameManager::score = 0;
    GameManager::currentLevel += 1;
    if (GameManager::ball != 0) setBallOnTee();
}

unsigned GameManager::hitCount(){
	return GameManager::hit;
}

void GameManager::enterPreviousLevel() {
    if (GameManager::currentLevel - 1 >= levels.size()) {
        std::cerr << "GameManager::enterNextLevel: unable to find previous level" << std::endl;
        return;
    }

    GameManager::currentLevel -= 1;
    if (GameManager::ball != 0) setBallOnTee();
}

int GameManager::getCurrentPossibleScore(){
	unsigned par = GameManager::getCurrentPar();
	int value = (hole_on_1 - (hit - 1) * (hole_on_1 - on_par)/(par -1));
	if(value < 1) value = 1;
	return value;
}

// Call the physics update and manage score.

void GameManager::updateGame(float dt) {
    if (GameManager::gameDone) return;

    Physics::update(dt);

    if (GameManager::ball == 0) {
        std::cerr << "GameManager::updateGame: unable to update game, ball not configured" << std::endl;
        return;
    }

    if (GameManager::levels.size() == 0) {
        std::cerr << "GameManager::updateGame: unable to update game, course not configured" << std::endl;
        return;
    }

    Ball& ball = GameManager::getBall();
    Level& level = GameManager::getCurrentLevel();

	float threshold = 0.009;

	glm::vec3 velocity_vector = ball.getVelocity();
	float currentVel = glm::length(velocity_vector);


	if( currentVel <= threshold && ballMoving ) {
			glm::vec3 currentTileNormal = getTileNormal2(level.getTile(Physics::getBallTileID()));
			if(fabs(currentTileNormal.y) > 0.95) {
				GameManager::ballMoving = false;
				if (Input::getKeyPress(' ')) hitDisable = true;
				GameManager::hit++;
			}
	}
	

    if (!GameManager::ballMoving) {
        ball.setVelocity(glm::vec3(0, 0, 0));

        if (Input::getKeyPress('[')) GameManager::hitAngle -= dt;
        if (Input::getKeyPress(']')) GameManager::hitAngle += dt;

        GameManager::hitPower += dt;
        if (GameManager::hitPower > 4.0) GameManager::hitPower = 0.1;

        if (Input::getKeyPress(' ') && !hitDisable) {
            ball.setVelocity( glm::vec3(hitPower * sin(GameManager::hitAngle), 0.f, hitPower * cos(GameManager::hitAngle)) );
            GameManager::ballMoving = true;
            recentWarp = 0;
        }
    }

    if (hitDisable && !Input::getKeyPress(' ')) hitDisable = false;

    Sphere tempCup = Sphere(level.getCup().getPosition(), ball.getGeometry().getRadius() * 2.0);
    Sphere tempBall = Sphere(ball.getGeometry().getCenter(), ball.getGeometry().getRadius());

    if (tempBall.collides(tempCup)) {
        int add = GameManager::getCurrentPossibleScore();
        GameManager::score += add;
        GameManager::TotalScore += GameManager::score;
		//std::cout << GameManager::score << std::endl;
		//std::cout << GameManager::getCurrentPar() << std::endl;

        if (GameManager::currentLevel < GameManager::levels.size() - 1)
            GameManager::enterNextLevel();
        else GameManager::gameDone = true;


		// If the game is finished, copy the priority queue to an array and write to file

		if(GameManager::gameDone){
			player_score ptemp = player_score(GameManager::playername, GameManager::TotalScore);
			hi_score::push(ptemp);
			hi_score::queue2array();
			hi_score::enqueueAll();
			hi_score::writefile();
		}


        hitDisable = true;
    }

    if (GameManager::ballMoving) {
        for (int wIter = 1; wIter <= GameManager::getCurrentLevel().getWarpCount(); ++wIter) {
            const Warp& warp = GameManager::getCurrentLevel().getWarp(wIter);

            Sphere tempWarpA = Sphere(warp.getPositionA(), ball.getGeometry().getRadius() * 2.0);
            Sphere tempWarpB = Sphere(warp.getPositionB(), ball.getGeometry().getRadius() * 2.0);

            if (recentWarp != wIter) {
                if (tempBall.collides(tempWarpA)) {
                    Physics::setBallTileID(warp.getTileIDb());
                    ball.setCenter( warp.getPositionB() + glm::vec3(0.f, ball.getGeometry().getRadius(), 0.f) );
                    recentWarp = wIter;
                    continue;
                }

                if (tempBall.collides(tempWarpB)) {
                    Physics::setBallTileID(warp.getTileIDa());
                    ball.setCenter( warp.getPositionA() + glm::vec3(0.f, ball.getGeometry().getRadius(), 0.f) );
                    recentWarp = wIter;
                    continue;
                }
            }
        }
    }
}
