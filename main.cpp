#include <iostream>
#include <cstdlib>
#include "glut.h"

using namespace std;

// how to use GLM like GLSL, can also prefix GLM stuff with glm::
#include "glm/glm.hpp"
using namespace glm;

#include "Sphere.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define SCREEN_FPS 60

// store window ID from glutCreateWindow, can be destroyed later
int winId;

void runTests() {
    cout << "Running a test of GLM vectors..." << endl;

    vec3 a = vec3(1.f, 2.f, 3.f);
    vec3 b = vec3(4.f, 5.f, 6.f);
    vec3 c = cross(a, b);

    cout << "c = <" << c.x << ", " << c.y << ", " << c.z << ">" << endl;

    cout << "Running a test of Sphere class..." << endl;

    Sphere A = Sphere(vec3(0.f, 0.f, 0.f), 5.f);
    Sphere B = Sphere(vec3(0.f, 7.f, 0.f), 1.f);

    cout << boolalpha << "A.collides(B) = " << A.collides(B) << endl;
}

void init() {
    glClearColor(0.f, 0.f, 0.5f, 1.f);

    runTests();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70, 1.333, 1, 100);

    glMatrixMode(GL_MODELVIEW);
}

void close() {
    glutDestroyWindow(winId);
    exit(0);
}

// display test: draw a plane facing the camera
void display() {
    glClear(GL_COLOR_BUFFER_BIT);

    glLoadIdentity();
    glTranslatef(0.f, 0.f, -5.f);
    glScalef(2.f, 2.f, 2.f);

    glBegin(GL_QUADS);
        glVertex3f(-1.f, +1.f, 0.f);
        glVertex3f(-1.f, -1.f, 0.f);
        glVertex3f(+1.f, -1.f, 0.f);
        glVertex3f(+1.f, +1.f, 0.f);
    glEnd();

    glutSwapBuffers();
}

// reshape function added to prevent user from resizing window
void reshape(int width, int height) {
    glutReshapeWindow(SCREEN_WIDTH, SCREEN_HEIGHT);
}

// close if escape key pressed
void keyboard(unsigned char key, int x, int y) {
    if (key == 27) close();
}

// timer function used to regulate frames per second (request redraw often)
// delay in milliseconds between frames = 1000 / frames per second
void timer(int stuff) {
    glutPostRedisplay();
    glutTimerFunc(1000 / SCREEN_FPS, timer, 0);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    winId = glutCreateWindow("Program Test");

    init();

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutTimerFunc(1000 / SCREEN_FPS, timer, 0);

    glutMainLoop();
    return 0;
}
