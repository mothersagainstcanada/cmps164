#ifndef _TEE_H
#define _TEE_H

#include "util.h" // includes GLM, string, vector

class Tee {
    int tileID;

    glm::vec3 position;

public:
    Tee();
    Tee(int tileID, float x, float y, float z);
    Tee(int tileID, const glm::vec3& position);
    Tee(const std::vector<std::string>& tokens);

    Tee& operator=(const Tee& rhs);

    void parseLine(const std::vector<std::string>& tokens);

    int getTileID() const ;
    const glm::vec3& getPosition() const;

    void clear();
};

#endif
