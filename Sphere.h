#ifndef _SPHERE_H
#define _SPHERE_H

#include "util.h" // includes GLM, string, vector

//taken straight from the UML

#include "Edge.h"

class Sphere {
    glm::vec3 center;
    float radius;
public:
    Sphere(const glm::vec3& _center, float _radius);
    bool collides(const Sphere&);
    bool collides(const Edge&) const;
    glm::vec3 intersection(const Edge&) const;

    const glm::vec3& getCenter() const;
    float getRadius() const;

    void setCenter(const glm::vec3&);
    void setRadius(float);
};

#endif
