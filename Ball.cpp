#include "Ball.h"

Ball::Ball(float radius):
    geometry(glm::vec3(0.f), radius)
{    this->velocity = glm::vec3(0.f, 0.f, 0.f);
}

Ball::Ball(const glm::vec3& center, float radius):
    geometry(center, radius)
{
}

const Sphere& Ball::getGeometry() const {
    return this->geometry;
}

const glm::vec3& Ball::getVelocity() const {
    return this->velocity;
}

void Ball::setVelocity(const glm::vec3& velocity) {
    this->velocity = velocity;
}

void Ball::setCenter(const glm::vec3& center) {
    this->geometry.setCenter(center);
}

/*
    BUG LOG:
    dt is capped at 0.1f to prevent large jumps in Ball position (and leaving of level)
    >THIS WILL SOMETIMES STOP RENDERING/UPDATING ALL TOGETHER, CAUSE UNKNOWN
    sometimes the Ball struggles to track its own tile ID, call to findTile made
    >if window dragged before Ball hits Edge, Ball will escape, crash program
    implemented a check to see if ball passes wall entirely for higher velocities
    also implemented code to place ball back into nearest tile if it exits
*/

void Ball::update(float dt) {
    this->geometry.setCenter(this->geometry.getCenter() + this->velocity * dt);
}
