#ifndef _CUP_H
#define _CUP_H

#include "util.h" // includes GLM, string, vector

class Cup {
    int tileID;

    glm::vec3 position;

public:
    Cup();
    Cup(int tileID, float x, float y, float z);
    Cup(int tileID, const glm::vec3& position);
    Cup(const std::vector<std::string>& tokens);

    Cup& operator=(const Cup& rhs);

    void parseLine(const std::vector<std::string>& tokens);

    int getTileID() const;
    const glm::vec3& getPosition() const;

    void clear();
};

#endif
