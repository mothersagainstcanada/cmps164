#ifndef _CAMERA_H
#define _CAMERA_H

#include "util.h" // includes GLM, string, vector

#include "Input.h"

#include <iostream>


// three different camera modes, default is FREE

enum VIEW_MODE{FREE, TOP, FOLLOW};

class Camera {
    
	glm::vec3 position;
	glm::mat4 view_matrix;
	double angleX;
	double angleY;
	double zoom;
	VIEW_MODE m;



public:

	Camera(){
		position = glm::vec3(0, 0, 0);
		angleX = 0;
		angleY = 0;
		zoom = 0;
		m = FREE;
	}

	Camera(glm::vec3 p, double x, double y){
		position = p;
		angleX = x;
		angleY = y;
		zoom = 0;
		m = FREE;
	}

	void setMode(VIEW_MODE M){
		m = M;
	}

	VIEW_MODE getMode(){
		return m;
	}

	void update(){

		bool right = Input::getKeyPress('d');
		bool left = Input::getKeyPress('a');
		bool up = Input::getKeyPress('q');
		bool down = Input::getKeyPress('e');
		bool backward = Input::getKeyPress('s');
		bool forward = Input::getKeyPress('w');

		int dmouseX = Input::getMouseDX();
		int dmouseY = Input::getMouseDY();

		double cx,cy,cz, speed;
		if(m == FREE){
				speed = .05;

				cx = speed * ((int)(right) - (int)(left));
				cy = speed * ((int)(up) - (int)(down));
				cz = speed * ((int)(backward) - (int)(forward));

				position.x += cx;
				position.y += cy;
				position.z += cz;
				angleX += dmouseY;
				angleY += dmouseX;


				view_matrix = glm::mat4(1);

				translate(-position.x, -position.y, -position.z, view_matrix);
				rotateY(angleY, view_matrix);
				rotateX(angleX, view_matrix);
				
		}
		if(m == TOP){

				speed = .05;

				cx = speed * ((int)(left) - (int)(right));
				cz = speed * ((int)(up) - (int)(down));
				cy = speed * ((int)(forward) - (int)(backward));
				
				position.x += cx;
				position.y += cy;
				position.z += cz;

				view_matrix = glm::mat4(1);

				rotateX(90, view_matrix);
				translate(-position.x, -position.y, -position.z, view_matrix);

		}


	}

	void update(glm::vec3 center, double radius){

		bool right = Input::getKeyPress('d');
		bool left = Input::getKeyPress('a');
		bool up = Input::getKeyPress('q');
		bool down = Input::getKeyPress('e');
		bool backward = Input::getKeyPress('s');
		bool forward = Input::getKeyPress('w');

		int dmouseX = Input::getMouseDX();
		int dmouseY = Input::getMouseDY();

		if(m != FOLLOW) return;
		angleX += dmouseY;
		angleY += dmouseX;


		double speed = .05;

		double cz = speed * ((int)(backward) - (int)(forward));
		zoom += cz;
		
		view_matrix = glm::mat4(1);
		translate(-center.x, -center.y, -center.z, view_matrix);
		rotateY(angleY, view_matrix);
		rotateX(angleX, view_matrix);
		translate(0, 0, -(radius * 3 + zoom), view_matrix);
		
	}

	
	const glm::mat4& getViewMatrix(){
		return view_matrix;
	}


    
};

#endif