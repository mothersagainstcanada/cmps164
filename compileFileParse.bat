::modify environment variable PATH temporarily
set PATH=%PATH%;C:\MinGW\bin;C:\Programs\MinGW\bin

::temporary variable to hold source files
set CPP_SOURCES=fileParse.cpp Tile.cpp Tee.cpp Cup.cpp util.cpp Level.cpp Ball.cpp Sphere.cpp Edge.cpp Input.cpp Render.cpp GameManager.cpp Physics.cpp HUD.cpp highscore.cpp Warp.cpp

::compiler commands; y'know, the usual
g++ -D_STDCALL_SUPPORTED -o minigolf.exe %CPP_SOURCES% -lopengl32 -lglu32 -l:glut32.lib

::let me read them errors/warnings
pause
