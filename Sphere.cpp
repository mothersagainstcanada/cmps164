#include "Sphere.h"

Sphere::Sphere(const glm::vec3& _center, float _radius) {
    this->center = _center;
    this->radius = _radius;
}

bool Sphere::collides(const Sphere& s) {
    float distance = glm::length(this->center - s.center);

    if (distance > this->radius + s.radius) return false;
    return true;
}

bool Sphere::collides(const Edge& e) const {
    glm::vec3 diff = e.NearestPoint(this->center) - this->center;

    return (glm::length(diff) < this->radius);
}

glm::vec3 Sphere::intersection(const Edge& e) const {
    return e.NearestPoint(this->center);
}

const glm::vec3& Sphere::getCenter() const {
    return this->center;
}

float Sphere::getRadius() const {
    return this->radius;
}

void Sphere::setCenter(const glm::vec3& v) {
    this->center = v;
}

void Sphere::setRadius(float r) {
    this->radius = r;
}
