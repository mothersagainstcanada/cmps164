#include "Warp.h"

#include <cstdlib>

Warp::Warp() {
    this->tileIDa = 0;
    this->positionA = glm::vec3(0.f);

    this->tileIDb = 0;
    this->positionB = glm::vec3(0.f);
}

Warp::Warp(int tileIDa, const glm::vec3& positionA, int tileIDb, const glm::vec3& positionB) {
    this->tileIDa = tileIDa;
    this->positionA = positionA;

    this->tileIDb = tileIDb;
    this->positionB = positionB;
}

Warp::Warp(const std::vector<std::string>& tokens) {
    this->tileIDa = atoi(tokens[1].c_str());
    this->tileIDb = atoi(tokens[2].c_str());

    this->positionA.x = atof(tokens[3].c_str());
    this->positionA.y = atof(tokens[4].c_str());
    this->positionA.z = atof(tokens[5].c_str());

    this->positionB.x = atof(tokens[6].c_str());
    this->positionB.y = atof(tokens[7].c_str());
    this->positionB.z = atof(tokens[8].c_str());
}

Warp& Warp::operator=(const Warp& rhs) {
    this->tileIDa = rhs.tileIDa;
    this->positionA = rhs.positionA;

    this->tileIDb = rhs.tileIDb;
    this->positionB = rhs.positionB;

    return *this;
}

void Warp::parseLine(const std::vector<std::string>& tokens) {
    *this = Warp(tokens);
}

int Warp::getTileIDa() const {
    return this->tileIDa;
}

const glm::vec3& Warp::getPositionA() const {
    return this->positionA;
}

int Warp::getTileIDb() const {
    return this->tileIDb;
}

const glm::vec3& Warp::getPositionB() const {
    return this->positionB;
}

void Warp::clear() {
    *this = Warp();
}
