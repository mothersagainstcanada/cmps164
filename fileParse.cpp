#include <cstdlib>
#include <ctime>

#include <iostream>

//using namespace std; // can include for more sane coding

#include "glut.h"

#include "glm/glm.hpp"

#include "Render.h"
#include "Physics.h"

#include "Input.h"
#include "GameManager.h"

#include "Level.h"
#include "Camera.h"
#include "Ball.h"

#include "util.h"
#include "highscore.h"

//using namespace glm; // can include for more sane coding

int winId;
unsigned int gameTimer;
float averageFrameTime;
float responsiveness = 0.5f;

float dt;

Ball testBall = Ball(0.03f);

// initialize the rendering.

void init() {
    glClearColor(0.f, 0.f, 0.5f, 1.f);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70, 1.333, 0.1, 30);

    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_COLOR_MATERIAL);

    gameTimer = clock();
    averageFrameTime = 1000.f / SCREEN_FPS;
    dt = averageFrameTime * 0.001f;

    GameManager::setBall(testBall);

    Render::init(0.f, 1.f, 0.f, 0.f);
}

void timer(int stuff) {
    float delayInMS = (float)clock() - (float)gameTimer + 5.f;
    if (delayInMS < 0.f) delayInMS = 0.f;

    averageFrameTime = responsiveness * delayInMS + (1.f - responsiveness) * averageFrameTime;

    int timerDelay = 1000 / SCREEN_FPS - (int)(averageFrameTime + 0.5f);
    if (timerDelay < 0) timerDelay = 0;

    glutPostRedisplay();
    glutTimerFunc(timerDelay, timer, 0);

    dt = averageFrameTime * 0.001f;
    gameTimer = clock();

    GameManager::updateGame(dt);
}

int main(int argc, char** argv) {
    if(argc < 2) {
        std::cerr << "Usage: Minigolf filename" << std::endl;
        return EXIT_FAILURE;
    }

	// get player name if provided.
	if(argc > 2) GameManager::setPlayerName(argv[2]);

    GameManager::loadCourse(argv[1]);

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);

    winId = glutCreateWindow("Minigolf");

	//read the high score file.
	hi_score::readfile("high_score.db");

    init();

    glutDisplayFunc(Render::cb_display);

    glutTimerFunc(1000 / SCREEN_FPS, timer, 0);

    glutKeyboardFunc(Input::keyboardFunc);
    glutKeyboardUpFunc(Input::keyboardUpFunc);
    glutMotionFunc(Input::motionFunc);
    glutPassiveMotionFunc(Input::passiveMotionFunc);

    glutMainLoop();
    return 0;
}
