#ifndef _HISCORE_H
#define _HISCORE_H

//#include "Render.h"
#include "GameManager.h"

#include <iostream>
#include <string>
#include <iterator>
#include <sstream>
#include <fstream>
#include <cstdio>

#include <queue>

class player_score{
	std::string name;
	int score;
	
public:
	player_score(){}

	player_score(std::string n, int sc){
		name = n; score = sc;
	}

	std::string getName(){
		return name;
	}

	int getScore(){
		return score;
	}
};

class compare_score{
public:
	bool operator()(player_score& p1, player_score& p2)
    {
		return (p1.getScore() < p2.getScore());
    }
};

class hi_score{
	
	static std::priority_queue<player_score, std::vector<player_score>, compare_score> pq;
	static player_score hi_array[];

public:
	static void push(player_score& ps){
		pq.push(ps);
	}

	static player_score top(){
		return pq.top();
	}

	static void pop(){
		pq.pop();
	}

	static void readfile(std::string);
	static void writefile();

	static void queue2array(){
		for(int i = 0; i < 10; ++i){
			hi_array[i] = pq.top();
			pq.pop();
		}
	}

	static void enqueueAll(){
		for(int i = 0; i < 10; ++i){
			pq.push(hi_array[i]);
		}
	}

	static player_score* getArray(){
		return hi_array;
	}

};

#endif

