#include "Edge.h"

Edge::Edge(const glm::vec3& _pointA, const glm::vec3& _pointB) {
    this->pointA = _pointA;
    this->pointB = _pointB;
}

glm::vec3 Edge::intersection(const Edge& e) {
    return glm::vec3(0.f); // IMPLEMENT (why?)
}

bool Edge::collides(const Edge& e) {
    return false; // IMPLEMENT (also, why?)
}

glm::vec3 Edge::NearestPoint(const glm::vec3& _point) const {
    glm::vec3 diff = this->pointA - _point;
    glm::vec3 normal = this->getNormal();

    glm::vec3 fromPoint = glm::dot(diff, normal) * normal;
    return (_point + fromPoint);
}

const glm::vec3& Edge::getPointA() const {
    return this->pointA;
}

const glm::vec3& Edge::getPointB() const {
    return this->pointB;
}

void Edge::setPointA(const glm::vec3& v) {
    this->pointA = v;
}

void Edge::setPointB(const glm::vec3& v){
    this->pointB = v;
}

glm::vec3 Edge::getNormal() const {
    glm::vec3 pointC = this->pointB + glm::vec3(0.f, 0.1f, 0.f);

    glm::vec3 lineAB = this->pointB - this->pointA;
    glm::vec3 lineAC = pointC - this->pointA;

    return glm::normalize(glm::cross(lineAB, lineAC));
}
