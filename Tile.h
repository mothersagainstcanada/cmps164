#ifndef _TILE_H
#define _TILE_H

#include "util.h" // includes GLM, string, vector

class Tile {
    int ID;
    unsigned vertexCount;

    std::vector<glm::vec3> vertices;
    std::vector<int> neighbors;

public:
    Tile();
    Tile(const std::vector<std::string>& tokens);

    void parseLine(const std::vector<std::string>& tokens);

    int getID() const;
    unsigned getVertexCount() const;

    const glm::vec3& getVertex(unsigned index) const;
    void getVertices(std::vector<glm::vec3>& output) const;

    int getNeighbor(unsigned index) const;
    void getNeighbors(std::vector<int>& output) const;

    void clear();
};

const Tile nullTile = Tile(); // added for ability to return a const reference (see Level.getTile)

#endif
