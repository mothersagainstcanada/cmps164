#include "util.h"

#include <sstream>




void split(const std::string& line, char delim, std::vector<std::string>& elems) {
    std::stringstream lineStream(line);
    std::string token;

    //elems.clear();

    while (std::getline(lineStream, token, delim)) {
        if (token.length() > 0) elems.push_back(token);
    }
}

void translate(double x, double y, double z, glm::mat4& vmatrix){
	glm::mat4 TheMatrix;
	TheMatrix[0] = glm::vec4(1, 0, 0, 0);
	TheMatrix[1] = glm::vec4(0, 1, 0, 0);
	TheMatrix[2] = glm::vec4(0, 0, 1, 0);
	TheMatrix[3] = glm::vec4(x, y, z, 1);
	vmatrix = TheMatrix * vmatrix;
}

void rotateX(double degree, glm::mat4& vmatrix){
	double radian = degree * 3.1415926535897932384626433832795/180.0;

	glm::mat4 TheMatrix;
	TheMatrix[0] = glm::vec4(1, 0, 0, 0);
	TheMatrix[1] = glm::vec4(0, cos(radian), sin(radian), 0);
	TheMatrix[2] = glm::vec4(0, -sin(radian), cos(radian), 0);
	TheMatrix[3] = glm::vec4(0, 0, 0, 1);

	vmatrix = TheMatrix * vmatrix;
}

void rotateY(double degree, glm::mat4& vmatrix){
	double radian = degree * 3.1415926535897932384626433832795/180.0;

	glm::mat4 TheMatrix;
	TheMatrix[0] = glm::vec4(cos(radian), 0, -sin(radian), 0);
	TheMatrix[1] = glm::vec4(0, 1, 0, 0);
	TheMatrix[2] = glm::vec4(sin(radian), 0, cos(radian), 0);
	TheMatrix[3] = glm::vec4(0, 0, 0, 1);
		
	vmatrix = TheMatrix * vmatrix;
}

glm::vec3 reflect(const glm::vec3& vector, const glm::vec3& normal) {
    glm::vec3 n = glm::normalize(normal);

    return vector - 2.f * glm::dot(vector, n) * n;
}
 