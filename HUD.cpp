#include "HUD.h"

using namespace std;

string HUD::score = "Score: ";
string HUD::total = "Total Score: ";

string HUD::par = "Par: ";
string HUD::stroke = "Stroke: ";


// concat numbers to the respective strings of the UI

void HUD::concatScore(int score){
	HUD::score = "Score: ";
	std::ostringstream oss;
	oss << score;
	HUD::score += oss.str();
}

void HUD::concatTotal(int tot){
	HUD::total = "Total Score: ";
	std::ostringstream oss;
	oss << tot;
	HUD::total += oss.str();
}

void HUD::concatPar(int par){
	HUD::par = "Par: ";
	std::ostringstream oss;
	oss << par;
	HUD::par += oss.str();
}

void HUD::concatStroke(int stroke){
	HUD::stroke = "Stroke: ";
	std::ostringstream oss;
	oss << stroke;
	HUD::stroke += oss.str();
}


// draw the HUD

void HUD::draw(){
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluOrtho2D(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0);

    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3d(1.0, 1.0, 1.0);


	// Position to draw text

	glRasterPos2i(SCREEN_WIDTH - 300, 10);


	// print text to the print by one character at a time
    for (string::iterator i = HUD::score.begin(); i != HUD::score.end(); ++i)
	{
	  char c = *i;
	  glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
	}

	glRasterPos2i(SCREEN_WIDTH - 150, 10);

	for (string::iterator i = HUD::total.begin(); i != HUD::total.end(); ++i)
	{
	  char c = *i;
	  glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
	}


	glRasterPos2i(20, 10);

	std::string name = GameManager::getPlayerName();
	for (string::iterator i = name.begin(); i != name.end(); ++i)
	{
	  char c = *i;
	  glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
	}

	glRasterPos2i(20, 30);

    for (string::iterator i = HUD::par.begin(); i != HUD::par.end(); ++i)
	{
	  char c = *i;
	  glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
	}

	glRasterPos2i(120, 30);

    for (string::iterator i = HUD::stroke.begin(); i != HUD::stroke.end(); ++i)
	{
	  char c = *i;
	  glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
	}
}

// Same as function draw(), printing high score

void HUD::hiscore(){
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluOrtho2D(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0);

    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3d(1.0, 1.0, 1.0);

	for(int i = 0; i < 10;){
		glRasterPos2i(SCREEN_WIDTH - 500, 100 + 40 * i);
		
		std::ostringstream oss;
		std::ostringstream oss2;

		player_score* ps = hi_score::getArray();

		std::string temp = "";
		oss << ++i;
		temp += oss.str(); temp += " ";
		temp += ps[i-1].getName();

		for (string::iterator it = temp.begin(); it != temp.end(); ++it)
		{
			char c = *it;
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
		}

		glRasterPos2i(SCREEN_WIDTH - 300, 100 + 40 * (i-1) );
		oss2 << ps[i-1].getScore();
		std::string temp2 = "";
		temp2 += oss2.str();

		for (string::iterator it = temp2.begin(); it != temp2.end(); ++it)
		{
			char c = *it;
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, c);
		}

	}

}