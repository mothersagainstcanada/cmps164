#include "Render.h"

Camera Render::View;
float Render::lightpos[4];


// Set the light source and camera initual position

void Render::init(float x, float y, float z, float w) {
    Render::View = Camera(glm::vec3(1,1,5),0,0);
	Render::lightpos[0] = x;
	Render::lightpos[1] = y;
	Render::lightpos[2] = z;
	Render::lightpos[3] = w;

    GameManager::setCamera(Render::View);
}


void drawLevel(const Level& l) {
    glColor3f(0.f, 0.5f, 0.f);

    for (int tIter = 0; tIter < l.getTileCount(); ++tIter) {
        const Tile& t = l.getTile(tIter + 1);

        glm::vec3 AB = t.getVertex(1) - t.getVertex(0);
        glm::vec3 AC = t.getVertex(2) - t.getVertex(0);

        glm::vec3 normal = glm::normalize(glm::cross(AB, AC));

        glNormal3f(normal.x, normal.y, normal.z);

        glBegin(GL_POLYGON);
            for (int vIter = 0; vIter < t.getVertexCount(); ++vIter) {
                const glm::vec3& v = t.getVertex(vIter);

                glVertex3f(v.x, v.y, v.z);
            }
        glEnd();
    }

    const glm::vec3& teePos = l.getTee().getPosition();
    glColor3f(0.5f, 0.5f, 0.5f);

    glPushMatrix();
    glTranslatef(teePos.x, teePos.y + 0.01, teePos.z);
	glScalef( 0.5/sqrt(3), 0, 0.5/sqrt(3));
	glNormal3f(0, 1, 0);
	glBegin(GL_TRIANGLES);
//    glutSolidSphere(0.1, 50, 50);
	glVertex3f(-1.f/2.f, 0, sqrt(3)/2.f);
	glVertex3f(-1.f/2.f, 0, -sqrt(3)/2.f);
	glVertex3f(1.f, 0, 0);
	glEnd();
    glPopMatrix();

    const glm::vec3& cupPos = l.getCup().getPosition();
    glColor3f(0.f, 0.f, 0.f);

    glPushMatrix();
    glTranslatef(cupPos.x, cupPos.y, cupPos.z);
    glutSolidSphere(2.f * GameManager::getBall().getGeometry().getRadius(), 50, 50);
    glPopMatrix();

}

void drawBall(const Ball& b) {
    const glm::vec3& center = b.getGeometry().getCenter();
    float radius = b.getGeometry().getRadius();

    glColor3f(1.f, 1.f, 1.f);

    glPushMatrix();
    glTranslatef(center.x, center.y, center.z);
    glutSolidSphere(radius, 50, 50);
    glPopMatrix();
}

void drawBallHit(const Ball& b) {
    const glm::vec3& center = b.getGeometry().getCenter();
    float hitAngle = GameManager::getHitAngle();
    float hitPower = GameManager::getHitPower();

    glPushMatrix();
    glTranslatef(center.x, center.y, center.z);

    glColor3f(1.f, 0.f, 0.f);

    glBegin(GL_LINES);
    glVertex3f(0.f, 0.f, 0.f);
    glVertex3f(hitPower * sin(hitAngle), 0.f, hitPower * cos(hitAngle));
    glEnd();

    glPopMatrix();
}

void drawEdges(const Level& l) {
    for (int tIter = 0; tIter < l.getTileCount(); ++tIter) {
        const Tile& t = l.getTile(tIter + 1);

        glColor3f(0.f, 0.f, 0.f);

        for (int nIter = 0; nIter < t.getVertexCount(); ++nIter) {
            unsigned neighborID = t.getNeighbor(nIter);

            if (neighborID == 0) {
                const glm::vec3& vertexA = t.getVertex(nIter);
                const glm::vec3& vertexB = t.getVertex((nIter + 1) % (t.getVertexCount()));

                glBegin(GL_QUADS);
                    glVertex3f(vertexA.x, vertexA.y, vertexA.z);
                    glVertex3f(vertexA.x, vertexA.y + 0.2f, vertexA.z);
                    glVertex3f(vertexB.x, vertexB.y + 0.2f, vertexB.z);
                    glVertex3f(vertexB.x, vertexB.y, vertexB.z);
                glEnd();
            }
        }
    }
}

void drawLevelWarps(const Level& l) {
    glColor3f(0.5f, 0.f, 0.5f);

    for (int wIter = 1; wIter <= l.getWarpCount(); ++wIter) {
        const Warp& w = l.getWarp(wIter);

        glPushMatrix();
        glTranslatef(w.getPositionA().x, w.getPositionA().y, w.getPositionA().z);
        glutSolidSphere(2.f * GameManager::getBall().getGeometry().getRadius(), 50, 50);
        glPopMatrix();

        glPushMatrix();
        glTranslatef(w.getPositionB().x, w.getPositionB().y, w.getPositionB().z);
        glutSolidSphere(2.f * GameManager::getBall().getGeometry().getRadius(), 50, 50);
        glPopMatrix();

        glBegin(GL_LINES);
        glVertex3f(w.getPositionA().x, w.getPositionA().y + 0.01f, w.getPositionA().z);
        glVertex3f(w.getPositionB().x, w.getPositionB().y + 0.01f, w.getPositionB().z);
        glEnd();
    }
}

void Render::cb_display(){
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70, 1.333, 0.1, 30);

    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_COLOR_MATERIAL);

    
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (Input::getKeyPress('1')) Render::View.setMode(FREE);
	if (Input::getKeyPress('2')) Render::View.setMode(TOP);
	if (Input::getKeyPress('3')) Render::View.setMode(FOLLOW);

	if (Input::getKeyPress('.')) GameManager::enterNextLevel();
	if (Input::getKeyPress(',')) GameManager::enterPreviousLevel();


	const Sphere& b = GameManager::getBall().getGeometry();;

	switch(Render::View.getMode()){
		case FOLLOW:
			Render::View.update(b.getCenter(), b.getRadius()); break;
		default:
			Render::View.update(); break;
	}

	glLoadIdentity();
	glLoadMatrixf( (float*) &(Render::View.getViewMatrix()) );

	glLightfv(GL_LIGHT0, GL_POSITION, Render::lightpos);

//	glScalef(1.0, 1.0, -1.0);

    glEnable(GL_LIGHTING);
    drawLevel(GameManager::getCurrentLevel());
    drawBall(GameManager::getBall());

    glDisable(GL_LIGHTING);
    drawEdges(GameManager::getCurrentLevel());
    drawLevelWarps(GameManager::getCurrentLevel());

    if (!GameManager::isBallMoving()) {
        glDisable(GL_DEPTH_TEST);
        drawBallHit(GameManager::getBall());
        glEnable(GL_DEPTH_TEST);
    }

	HUD::concatScore(GameManager::getCurrentPossibleScore());
	HUD::concatTotal(GameManager::getTotalScore());
	HUD::concatPar(GameManager::getCurrentPar());
	HUD::concatStroke(GameManager::hitCount());

	glClear(GL_DEPTH_BUFFER_BIT);

	HUD::draw();

	if(GameManager::isGameDone()) HUD::hiscore();

    glutSwapBuffers();
    Input::clearMouseDiff();
}




