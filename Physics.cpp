#include "Physics.h"

#include <iostream>

#include "GameManager.h"
#include "Ball.h"
#include "Edge.h"
#include "Level.h"
#include "Sphere.h"
#include "Tile.h"
#include "util.h"

int Physics::ballTileID = 0;

float Physics::gravity = 5.f;
float Physics::friction = 1.f;
float Physics::impact = 0.5f;

/* helper functions prototypes */
glm::vec3 getTileCenter(const Tile& tile);
glm::vec3 getTileNormal(const Tile& tile);
bool isBallInsideTile(const Ball& ball, const Tile& tile);
int findTileWithBall(const Ball& ball, const Level& level);
int findClosestTileToBall(const Ball& ball, const Level& level);
float getTileHeight(const Tile& tile, float xCoord, float zCoord);

void Physics::update(float dt) {
    if (dt > 0.1f) dt = 0.1f;

    Ball& ball = GameManager::getBall();
    Level& level = GameManager::getCurrentLevel();

    bool resolved = false;
    bool tileMismatch = false;

    glm::vec3 ballVelocity = ball.getVelocity();
    glm::vec3 ballCenter = ball.getGeometry().getCenter();
    float ballRadius = ball.getGeometry().getRadius();

    ballVelocity.y = 0.f; // work with physics on a two-dimensional basis
    ballCenter.y = 0.f;

    const glm::vec3& cupPos = level.getCup().getPosition();
    Sphere cup = Sphere(glm::vec3(cupPos.x, -ballRadius, cupPos.z), 0.1f);

    if ( !isBallInsideTile(ball, level.getTile(Physics::ballTileID)) ) { // ball not in reported tile
        int tempID = findTileWithBall(ball, level);
        if (tempID == 0) tempID = findClosestTileToBall(ball, level);

        Physics::ballTileID = tempID;
        tileMismatch = true;
    }

    const Tile& tile = level.getTile(Physics::ballTileID);
    unsigned tileVertexCount = tile.getVertexCount();
    glm::vec3 tileCenter = getTileCenter(tile);
    glm::vec3 tileNormal = getTileNormal(tile);

    // apply gravity
    ballVelocity.x += Physics::gravity * tileNormal.x * dt;
    ballVelocity.z += Physics::gravity * tileNormal.z * dt;

    // apply friction
    ballVelocity.x += Physics::friction * (-ballVelocity.x) * dt;
    ballVelocity.z += Physics::friction * (-ballVelocity.z) * dt;

    for (int vertex = 0; vertex < tileVertexCount; ++vertex) {
        unsigned neighborID = tile.getNeighbor(vertex);

        const glm::vec3& pointA = tile.getVertex(vertex);
        const glm::vec3& pointB = tile.getVertex((vertex + 1) % tileVertexCount);

        Edge tempEdge = Edge(pointA, pointB);
        Ball tempBall = Ball(ballCenter, ballRadius);
        glm::vec3 edgeNormal = tempEdge.getNormal();
        glm::vec3 projectedCenter = ballCenter + ballVelocity * dt;

        if (neighborID == 0) {
            glm::vec3 nearestDiff = ballCenter - tempEdge.NearestPoint(ballCenter);
            glm::vec3 nearestProjectedDiff = projectedCenter - tempEdge.NearestPoint(projectedCenter);

            if (tempBall.getGeometry().collides(tempEdge)) {
                glm::vec3 nearest = tempBall.getGeometry().intersection(tempEdge);
                glm::vec3 diff = nearest - tempBall.getGeometry().getCenter();

                glm::vec3 tester = tempEdge.NearestPoint(tileCenter) - tileCenter;
                if (glm::dot(tester, diff) < 0.f) diff = -diff;

                ballCenter = glm::normalize(-diff) * 1.01f * ballRadius + nearest;
                ballVelocity = reflect(ballVelocity, edgeNormal);
                resolved = true;

                tempBall.setCenter(ballCenter);
                tempBall.setVelocity(ballVelocity);
            }

            else if (glm::dot(nearestDiff, nearestProjectedDiff) < 0.f) {
                float distNearest = glm::length(nearestDiff);
                float distProjected = glm::length(nearestProjectedDiff);

                glm::vec3 collisionPoint = ballCenter + (distNearest / (distNearest + distProjected)) * (projectedCenter - ballCenter);
                glm::vec3 reflectedVelocity = reflect(ballVelocity * dt, edgeNormal);

                ballCenter = collisionPoint + (distProjected / (distNearest + distProjected)) * reflectedVelocity;
                ballVelocity = reflectedVelocity / dt;
                resolved = true;

                tempBall.setCenter(ballCenter);
                tempBall.setVelocity(ballVelocity);
            }

            else if (tileMismatch) {
                glm::vec3 tileDiff = tileCenter - tempEdge.NearestPoint(ballCenter);

                if (glm::dot(nearestDiff, tileDiff) < 0.f) {
                    ballCenter = tempEdge.NearestPoint(ballCenter) - glm::normalize(nearestDiff) * ballRadius * 1.01f;
                    ballVelocity = reflect(ballVelocity, edgeNormal);
                    resolved = true;

                    tempBall.setCenter(ballCenter);
                    tempBall.setVelocity(ballVelocity);
                }
            }

        } else { // neighbor exists
            glm::vec3 toCurrentSphere = tempBall.getGeometry().getCenter() - tempBall.getGeometry().intersection(tempEdge);

            tempBall.setCenter(projectedCenter);
            glm::vec3 toProjectedSphere = tempBall.getGeometry().getCenter() - tempBall.getGeometry().intersection(tempEdge);

            if (glm::dot(toCurrentSphere, toProjectedSphere) < 0.f) {
                Physics::ballTileID = neighborID;
            }
        }
    }

    if (!resolved) ballCenter += ballVelocity * dt;

    ballCenter.y = getTileHeight(tile, ballCenter.x, ballCenter.z) + ballRadius;
    ball.setCenter(ballCenter);
    ball.setVelocity(ballVelocity);
}

int Physics::getBallTileID() {
    return Physics::ballTileID;
}

void Physics::setBallTileID(int ID) {
    Level& level = GameManager::getCurrentLevel();

    if (ID < 1 && ID > level.getTileCount()) {
        std::cerr << "Physics::setBallTileID: tile of ID " << ID << " not found" << std::endl;
        return;
    }

    Physics::ballTileID = ID;
}

float Physics::getGravity() {
    return Physics::gravity;
}

void Physics::setGravity(float gravity) {
    Physics::gravity = gravity;
}

float Physics::getFriction() {
    return Physics::friction;
}

void Physics::setFriction(float friction) {
    Physics::friction = friction;
}

float Physics::getImpact() {
    return Physics::impact;
}

void Physics::setImpact(float impact) {
    Physics::impact = impact;
}

glm::vec3 getTileCenter(const Tile& tile) {
    glm::vec3 center = glm::vec3(0.f);

    unsigned vertexCount = tile.getVertexCount();

    for (int vertex = 0; vertex < vertexCount; ++vertex) {
        center += tile.getVertex(vertex);
    }

    return (center / (float)vertexCount);
}

glm::vec3 getTileNormal(const Tile& tile) {
    glm::vec3 AB = tile.getVertex(1) - tile.getVertex(0);
    glm::vec3 AC = tile.getVertex(2) - tile.getVertex(0);

    return glm::normalize(glm::cross(AB, AC));
}

bool isBallInsideTile(const Ball& ball, const Tile& tile) {
    unsigned vertexCount = tile.getVertexCount();

    const glm::vec3& ballPos = ball.getGeometry().getCenter();
    glm::vec3 tileCenter = getTileCenter(tile);

    for (int vertex = 0; vertex < vertexCount; ++vertex) {
        const glm::vec3& pointA = tile.getVertex(vertex);
        const glm::vec3& pointB = tile.getVertex((vertex + 1) % vertexCount);

        Edge temp = Edge(pointA, pointB);

        glm::vec3 ballPosProj = temp.NearestPoint(ballPos);
        glm::vec3 tileCenterProj = temp.NearestPoint(tileCenter);

        if (glm::dot(ballPos - ballPosProj, tileCenter - tileCenterProj) < 0.f) return false;
    }

    return true;
}

int findTileWithBall(const Ball& ball, const Level& level) {
    unsigned tileCount = level.getTileCount();

    for (int id = 1; id <= tileCount; ++id) {
        const Tile& tile = level.getTile(id);

        if (isBallInsideTile(ball, tile)) return id;
    }

    return 0;
}

int findClosestTileToBall(const Ball& ball, const Level& level) {
    float minimumDistance = 1e100;
    int minimumDistanceID = 0;

    for (int id = 1; id <= level.getTileCount(); ++id) {
        const Tile& tile = level.getTile(id);
        glm::vec3 tileCenter = getTileCenter(tile);

        float distanceToBall = glm::length(ball.getGeometry().getCenter() - tileCenter);

        if (distanceToBall < minimumDistance) {
            minimumDistance = distanceToBall;
            minimumDistanceID = id;
        }
    }

    return minimumDistanceID;
}


float getTileHeight(const Tile& tile, float xCoord, float zCoord) {
    const glm::vec3& P0 = tile.getVertex(0);
    glm::vec3 tileNormal = getTileNormal(tile);

    float yCoord = (glm::dot(tileNormal, P0) - tileNormal.x * xCoord - tileNormal.z * zCoord) / tileNormal.y;

    return yCoord;
}
