#ifndef _UTIL_H
#define _UTIL_H

#include <string>
#include <vector>
#include <cmath>

#include "glm/glm.hpp"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define SCREEN_FPS 40

const glm::vec3 zeroVector = glm::vec3(0.f);

void split(const std::string& line, char delim, std::vector<std::string>& elems);

void translate(double, double, double, glm::mat4&);

void rotateX(double, glm::mat4&);

void rotateY(double, glm::mat4&);

glm::vec3 reflect(const glm::vec3& vector, const glm::vec3& normal);

#endif
