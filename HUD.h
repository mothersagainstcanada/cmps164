#ifndef _HUD_H
#define _HUD_H

#include "Render.h"
#include "GameManager.h"
#include "highscore.h"

#include <iostream>
#include <string>
#include <iterator>
#include <sstream>

class HUD{
	static std::string score; 
	static std::string total;
	static std::string par;
	static std::string stroke;

public:
	static void concatScore(int);
	static void concatTotal(int);
	static void concatPar(int);
	static void concatStroke(int);

	static void draw();
	static void hiscore();


};

#endif

