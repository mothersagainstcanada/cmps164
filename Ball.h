#ifndef _BALL_H
#define _BALL_H

#include "Sphere.h"

class Ball {
    Sphere geometry;
    glm::vec3 velocity;

public:
    Ball(float radius);
    Ball(const glm::vec3& center, float radius);

    const Sphere& getGeometry() const;
    const glm::vec3& getVelocity() const;

    void setVelocity(const glm::vec3& velocity);
    void setCenter(const glm::vec3& center);

    void update(float dt);
};

#endif
